#include <Arduino.h>
class Prox
{
	public:
	Prox(int out,int echo);					//initializam clasa Prox (trig pin , echo pin)
	Prox(int out,int echo[],int n);				//initializam clasa Prox (trig pin , echo pin)
	~Prox();						//destructor
	float dist()      const;				//return curent distance of element 0
	float dist(int n)      const;				//return curent distance of element n

	private:
	void init(int out,int echo[],int n); 					//constructor
	int out;								//pinul de iesire
	int *echo;								//lista de semnale echo
	int n;									//numarul de elemente
};