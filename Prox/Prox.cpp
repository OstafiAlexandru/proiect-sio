#include "Prox.h"
//consttruct/destruct
void Prox::init(int out,int echo[],int n)
{
	//Initializam pinii
	this->out = out;
	this->echo = echo;
	this->n = n;
	pinMode(this->out, OUTPUT);
	for(int i=0;i<n;i++){ pinMode(this->echo[i], INPUT); }
}
//
Prox::Prox(int out,int echo){this->init(out,new int[1]{echo},1);}
Prox::Prox(int out,int echo[],int n){this->init(out,echo,n);}
Prox::~Prox()
{
	~this->out;
	delete[] this->echo;
	~this->n;
}

//Functie pentru aflarea distantei in cm
float Prox::dist(int n) const
{
	digitalWrite(this->out, LOW);
	delayMicroseconds(2);
	
	//Un puls de 10uS trebuie creat pentru a incepe 
	//inregistrarea sunetelor conform datasheet
	digitalWrite(this->out, HIGH);
	delayMicroseconds(10);
	
	digitalWrite(this->out, LOW);
	
	//Dezactivam intreruperile
	noInterrupts();
	
	//Citim pulsul de echo inregistrat de senzor
	float d=pulseIn(this->echo[n], HIGH);
	
	//Activam intreruperile
	interrupts();
	
	//Impartim la 58 pentru a ajunge la centimetri 
	//conform datasheet
	return d / 58.0;
}
float Prox::dist() const{return this->dist(0);}
