#include <BH1750.h>
#include <Wire.h>
#include <Prox.h>
#include <LiquidCrystal.h>
#include <Wire.h>
#include "RTClib.h"

#define echoPin 0
#define trigPin 1
#define buzzer 2
#define laser 3

int password_sequence = 0;
bool is_armed = false;
//LCD pin to Arduino
const int pin_RS = 8;
const int pin_EN = 9;
const int pin_d4 = 4;
const int pin_d5 = 5;
const int pin_d6 = 6;
const int pin_d7 = 7;
const int pin_BL = 10;

RTC_DS3231 rtc;
DateTime lightSensorDate;

LiquidCrystal lcd( pin_RS,  pin_EN,  pin_d4,  pin_d5,  pin_d6,  pin_d7);
char daysOfTheWeek[7][12] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

BH1750 GY30; // instantiate a sensor event object
Prox hc(trigPin, echoPin);

void setup() {
  Serial.begin(9600); // launch the serial monitor
  Wire.begin(); // Initialize the I2C bus for use by the BH1750 library
  GY30.begin(); // Initialize the sensor object
  pinMode(buzzer, OUTPUT);
  pinMode(laser, OUTPUT);
  lcd.begin(16, 2);
  lcd.setCursor(0, 0);
  lcd.print("Press Key:");
  digitalWrite(laser, HIGH);
  digitalWrite(buzzer, HIGH);

  if (! rtc.begin()) {
    Serial.println("Couldn't find RTC");
    while (1);
  }

  if (rtc.lostPower()) {
    Serial.println("RTC lost power, lets set the time!");

    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  }

}
void loop() {
  float lux = GY30.readLightLevel(); // read the light level from the sensor and store it in a variable
  float dist = hc.dist();
  if(is_armed == true)
  {
  Password_Check();
  checkSensors(lux,dist);
  }
  else
  {
    digitalWrite(buzzer, HIGH);  
  }
  Password_Check();
  delay(10);
}

static void checkSensors(float lux,float dist){
  if(lux<100){
  Serial.print("Dioda(");
  Serial.print(lux);
  Serial.print(") ->");
  lightSensorDate = rtc.now();
  }
  if(dist<12 && dist>1){
    
  Serial.print("Ultrasonic(");
  Serial.print(dist);
  Serial.print(") -> ");
  DateTime now = rtc.now();
  Serial.print("secs: ");
  Serial.println(lightSensorDate.second() == now.second());
  if(now.day()==lightSensorDate.day() && now.hour()==lightSensorDate.hour() && now.minute()==lightSensorDate.minute() && lightSensorDate.second()<=now.second()){
    digitalWrite(buzzer, LOW);
  }
  }
  else {
    Serial.print("DISTANTA :");
    Serial.print(dist);
    }
}

static void Password_Check() {
  Serial.println(password_sequence);
  int x;
  x = analogRead (0);
  lcd.setCursor(0, 1);
  if (x < 60) {
    lcd.print ("Right         ");
    if(password_sequence==1){
      password_sequence = password_sequence+1;
    } else {
        password_sequence = 0;
      }
  }
  else if (x < 200) {
    lcd.print ("Up            ");
    if(password_sequence==3){
      password_sequence = password_sequence+1;
    } else {
        password_sequence = 0;
      }
  }
  else if (x < 400) {
    lcd.print ("Down          ");
    password_sequence = 0;
  }
  else if (x < 600) {
    lcd.print ("Left          ");
    if(password_sequence==0 || password_sequence==2){
      password_sequence = password_sequence+1;
    } else {
        password_sequence = 0;
      }
  }
  else if (x < 800) {
    lcd.print ("Select");
    if(password_sequence==4){
      password_sequence = password_sequence+1;
    } else {
        password_sequence = 0;
      }
  }
  if(password_sequence==5){
    is_armed = !is_armed;
    if(is_armed){
      lcd.setCursor(0, 0);
      lcd.print("Armed         ");
      lcd.setCursor(0, 1);
     }else{
      lcd.setCursor(0, 0);
      lcd.print("Disarmed      ");
      lcd.setCursor(0, 1);
     }
    lcd.print ("Parola Corecta  ");
    password_sequence = 0;
  }
}
