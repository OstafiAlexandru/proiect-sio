//Librarie senzor intensitate luminoasa
#include <BH1750.h>

//Librarie pentru I2C/TWI
#include <Wire.h>

//Librarie LCD
#include <LiquidCrystal.h>

//Librarie Timer
#include "RTClib.h"

//Librarie Senzor Ultrasonic
#include <Prox.h>

//Pinii digitali pentru senszorul Ultrasonic
#define echoPin 0
#define trigPin 1

//Pinul digital pentru buzzer
#define buzzer 2

//Pinul digital laser
#define laser 3

//Variabila pentru a tine cont de parola introdusa
int password_sequence = 0;

//Variablia care tine cont de counter pana cand suna alarma
int counterTarget = 10;

//Variabila pentru a tine cont de statusul armarii
bool is_armed = false;

//Variabila pentru a tine cont de timpul acordat pentru a introduce parola de dezarmare
int safeTime = 0;

//Variabila pentru a tine cont daca un buton este apasat
boolean buttonPressed = false;

//LCD pin to Arduino
const int pin_RS = 8;
const int pin_EN = 9;
const int pin_d4 = 4;
const int pin_d5 = 5;
const int pin_d6 = 6;
const int pin_d7 = 7;
const int pin_BL = 10;

//Declaram timerul
RTC_DS3231 rtc;

//Declaram ecranul LCD
LiquidCrystal lcd( pin_RS,  pin_EN,  pin_d4,  pin_d5,  pin_d6,  pin_d7);

//Declaram senzorul de intensitate luminoasa
BH1750 GY30;

//Declaram senzorul ultrasonic
Prox hc(trigPin, echoPin);

void setup() {
  Serial.begin(9600);           // Initializam monitorul serialei
  Wire.begin();                 // Initializam magistrala I2C
  GY30.begin();                 // Initializam senzorul de instensitate luminoasa
  initializeLaser();            // Initializam modulul laser
  initializeBuzzer();           // Initializam buzzer-ul
  initializeLCD();              // Initializam ecranul LCD
  initializeRealTimeClock();    // Initializam modulul RTC
}

void loop() {
  Password_Check();             // Verificam introducerea codului de armare/dezarmare
  if (is_armed)                 // Verificam daca sistemul este armat
    checkSensors();             // Activam senzorii
  else
    disarmAlarm();              // Dezactivam alarma
}

// Dezactivam buzzer ul si resetam timpul de activare
void disarmAlarm() {
  safeTime = 0;
  digitalWrite(buzzer, HIGH);
}

// Initializeaza laserul si il activeaza
void initializeLaser() {
  pinMode(laser, OUTPUT);
  digitalWrite(laser, HIGH);
}

// Initializam LCD ul si setam cursorul
void initializeLCD() {
  lcd.begin(16, 2);
  lcd.setCursor(0, 0);
  lcd.print("Tastati parola:");
}

// Initializam buzzer ul ca inactiv
void initializeBuzzer() {
  pinMode(buzzer, OUTPUT);
  digitalWrite(buzzer, HIGH);
}

// Initializam modulul RTC
void initializeRealTimeClock() {
  if (!rtc.begin()) { // Verificam daca este conectat modulul
    Serial.println("Couldn't find RTC");
    while (1);
  }
  if (rtc.lostPower()) { 
    Serial.println("RTC lost power, lets set the time!");
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  }
}

static void checkSensors() {
  float lux = GY30.readLightLevel();                    //Citim nivelul de intensitate luminoasa detectata
  float dist = hc.dist();                               //Citim distanta detectata
  Serial.print("dist ----->");
  Serial.println(dist);
  Serial.print("timer ----->");
  Serial.println(safeTime);
  if (isAlarmTriggered()) {                            //Verificam daca a fost activata alarma
    checkAlarmLogic();                                  //Incepem numaratoarea inversa
  } else if (isUltrasonicSensorTriggered(dist) && isLightSensorTriggered(lux)) {      //Daca ambii senzori detecteaza
    Serial.print("Dioda -> ");
    Serial.println(lux);
    Serial.print("ULTRASONIC ---->");
    delay(250);
    lux = GY30.readLightLevel();                        //Dupa o intarziere verificam din nou daca senzorul de lumina detecteaza ceva
    Serial.print("LUX ---->");
    Serial.println(lux);
    dist = hc.dist();                                                         //Dupa o intarziere verificam din nou daca senzorul de distanta detecteaza ceva
    Serial.println(dist);
    if (isUltrasonicSensorTriggered(dist) && !isLightSensorTriggered(lux)) {  // Daca senzorul de ultrasonic detecteaza dar cel de lumina nu
      triggerCounter();                                                       // Incepem numaratoarea inversa
      Serial.print("TIMP DIST --->");
      Serial.println(safeTime);
    }
  }
}

//Declansam numaratoarea inversa
void triggerCounter() {
  DateTime safeDateTime = rtc.now();
  safeTime = safeDateTime.hour() * 3600 + safeDateTime.minute() * 60 + safeDateTime.second();     //Calculam numarul de secunde din momentul declansarii
}

//Verificam daca este declansat senzorul de lumina
bool isLightSensorTriggered(int lux) {
  return lux < 100;
}

//Verificam daca este declansat senzorul de ultrasonic
bool isUltrasonicSensorTriggered(float dist) {
  return dist < 10 && dist > 0.5;
}

//Verificam daca este alarma declansata
bool isAlarmTriggered() {
  return safeTime != 0;
}

//Daca numaratoarea inversa s-a terminat activam alarma
void checkAlarmLogic() {
  Serial.print("A intrat in functie!");
  DateTime nowDateTime = rtc.now();
  int nowTime = nowDateTime.hour() * 3600 + nowDateTime.minute() * 60 + nowDateTime.second();     //Calculam numarul de secunde din momentul acesta
//  Serial.println(nowTime - safeTime);

  if (counterReachedTarget(nowTime, safeTime)) {          //Daca numaratoarea inversa s-a terminat
    triggerAlarm();                                       //Declansam alarma
  } else {
    setCounterOnLCD(nowTime, safeTime);                   //Afisam numaratoarea inversa pe ecran
  }
}

//Afisam numaratoarea inversa pe ecran
void setCounterOnLCD(int nowTime, int safeTime){
  lcd.setCursor(14, 0);
  lcd.print (nowTime - safeTime);
}

//Declansam alarma
void triggerAlarm() {
  digitalWrite(buzzer, LOW);
  safeTime = 0;
}

//Verificam daca numaratoarea inversa s-a terminat
bool counterReachedTarget(int nowTime, int safeTime){
  return nowTime - safeTime > counterTarget;
}

//Verificam introducerea codului de armare/dezarmare
static void Password_Check() {
  int x;
  x = analogRead (0);       //Citim valoarea modulului de butoane
  lcd.setCursor(0, 1);

  if (buttonPressed == false) {     //Daca nu este apasat deja un buton

    if (x < 50) {                   //A fost apasat butonul de dreapta
      lcd.print ("Dreapta         ");
      buttonPressed = true;
      if (password_sequence == 1) {       //Avansam counterul pentru introducerea unei parole sau il resetam
        password_sequence = password_sequence + 1;
      } else {
        password_sequence = 0;
      }
    }
    else if (x > 50 && x < 150) {          //A fost apasat butonul de sus
      lcd.print ("Sus             ");
      buttonPressed = true;
      if (password_sequence == 3) {       //Avansam counterul pentru introducerea unei parole sau il resetam
        password_sequence = password_sequence + 1;
      } else {
        password_sequence = 0;
      }
    }
    else if (x > 200 && x < 300) {             //A fost apasat butonul de jos
      lcd.print ("Jos             ");
      buttonPressed = true;
      password_sequence = 0;
    }
    else if (x > 400 && x < 500) {                //A fost apasat butonul de stanga
      lcd.print ("Stanga          ");
      buttonPressed = true;
      if (password_sequence == 0 || password_sequence == 2) {         //Avansam counterul pentru introducerea unei parole sau il resetam
        password_sequence = password_sequence + 1;
      } else {
        password_sequence = 0;
      }
    }
    else if (x > 550 && x < 700) {
      lcd.print ("Confirm         ");                       //A fost apasat butonul de confirmare
      buttonPressed = true;
      if (password_sequence == 4) {             //Avansam counterul pentru introducerea unei parole sau il resetam
        password_sequence = password_sequence + 1;
      } else {
        password_sequence = 0;
      }
    }
  }

  //La eliberarea unui buton
  if (x > 900) {
    buttonPressed = false;
  }

  //La introducerea codului de armare/dezarmare
  if (password_sequence == 5) {
    is_armed = !is_armed;
    if (is_armed) {
      lcd.setCursor(0, 0);
      lcd.print("Armat           ");
      lcd.setCursor(0, 1);
    } else {
      lcd.setCursor(0, 0);
      lcd.print("Dezarmat        ");
      lcd.setCursor(0, 1);
    }
    lcd.print ("Parola Corecta  ");
    password_sequence = 0;            //Resetam parola
  }
}
